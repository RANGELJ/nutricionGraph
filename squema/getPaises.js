module.exports = {
    type: "@List(Pais)",
    sqlfiles: ["getPaises"],
    async resolve(root, args, context) {
        // @Error al consultar los paises
        const conn = await context.db("nutricion");
        const [rows] = await conn.execute("sql@paises");
        conn.destroy();
        return rows;
    },
};
