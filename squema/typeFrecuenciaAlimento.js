module.exports = new GraphQLObjectType({
    name: "typeFrecuenciaAlimento",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
    }),
});
