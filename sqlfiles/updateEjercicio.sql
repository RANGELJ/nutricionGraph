
-- @updateEjercicio
UPDATE pacientes
SET
    idIntencidadFisica = ?,
    idFrecuenciaEjercicio = ?,
    idDuracionEjercicio = ?,
    fechaInicioEjercicio = ?,
    aspectoGeneral = ?
WHERE
    id = ?
-- @
