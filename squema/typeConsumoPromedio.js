module.exports = new GraphQLObjectType({
    name: "typeCostoPromedio",
    fields: () => ({
        nombre: {type: "@String"},
        consumo: {type: "@Float"},
    }),
});
