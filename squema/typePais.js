module.exports = new GraphQLObjectType({
    name: "typePais",
    sqlfiles: ["typePais"],
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        entidadesFederativas: {
            type: "@List(EntidadFederativa)",
            async resolve(pais, args, context) {
                // @Error al consultar las entidades federativas del pais
                const conn = await context.db("nutricion");
                const params = [pais.id];
                const [entidades] = await conn.execute("sql@entidades", params);
                conn.destroy();
                return entidades;
            },
        },
    }),
});
