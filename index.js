const express = require("express");
const expressGraphQL = require("express-graphql");
const http = require("http");

const app = express();
require("./local_modules/session_builder")(app);
const graphErrorLoger
    = require("./local_modules/error_loger")("graph_resolve_errors.log");

app.use("/", expressGraphQL(req => ({
    schema: require("./squema/zroot"),
    graphiql: true,
    context: {
        db: require("./local_modules/mysql/db"),
        logError: graphErrorLoger,
        session: req.session,
        throwError: (message) => {
            throw {noLog: true, message};
        },
    },
})));

http.createServer(app).listen(8383, () => {
    console.log("Server runing on port 8383... "); // eslint-disable-line
});
