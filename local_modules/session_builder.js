const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);
const sessionConfig = require("../mysql.conf").sessionConfig;

const maxAgeMilliseconds = sessionConfig.maxAgeMilliseconds;

const options = {
    connectionLimit: sessionConfig.connectionLimit,
    host: sessionConfig.host,
    port: sessionConfig.port,
    user: sessionConfig.user,
    password: sessionConfig.password,
    database: sessionConfig.database,
    // How frequently expired sessions will be cleared; milliseconds:
    checkExpirationInterval: 900000,
    // The maximum age of a valid session; milliseconds:
    expiration: maxAgeMilliseconds,
    schema: {
        tableName: "sessions",
        columnNames: {
            session_id: "session_id",
            expires: "expires",
            data: "data",
        },
    },
};

const sessionStore = new MySQLStore(options);

module.exports = function(app) {
    app.use(session({
        key: "T8*&X9hu9H_frrNVJ*c",
        secret: "aeyxEVwe%$+K#d%hrvZdN7&ukNF#t#yy9gA*@_3?2K4!hU%+7QY6!etHgb"
            + "g%RP89+dju86684?gdJE8Qag?SaLv2h4Pg$pC57uxzV",
        store: sessionStore,
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: maxAgeMilliseconds,
        },
        maxAge: maxAgeMilliseconds,
    }));
};
