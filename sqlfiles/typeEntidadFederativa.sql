
-- @municipios
SELECT
	id,
    nombre
FROM
	municipios
WHERE
	activo = 1
    AND idEntidadFederativa = ?
ORDER BY nombre
-- @