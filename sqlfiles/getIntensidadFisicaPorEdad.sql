
-- @intensidad
SELECT
	TIMESTAMPDIFF(YEAR,
        generales.fechaNacimiento,
        NOW()
    ) AS edad
FROM
	pacientes paciente
JOIN
    generales ON (
        generales.id = paciente.idGenerales
    )
WHERE
	paciente.activo = 1
    AND paciente.idIntencidadFisica = ?
HAVING
	edad BETWEEN ? AND ?
-- @