module.exports = new GraphQLObjectType({
    name: "typeEntidadFederativa",
    sqlfiles: ["typeEntidadFederativa"],
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        municipios: {
            type: "@List(Municipio)",
            async resolve(entidad, args, context) {
                // @Error al consultar los municipios
                const conn = await context.db("nutricion");
                const params = [entidad.id];
                const [municipios] = await conn.execute("sql@municipios",
                    params);
                conn.destroy();
                return municipios;
            },
        },
    }),
});
