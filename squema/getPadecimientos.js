module.exports = {
    type: "@List(Padecimiento)",
    args: {
        idPaciente: {type: "@Int"},
    },
    sqlfiles: ["getPadecimientos"],
    async resolve(root, args, context) {
        // @Error al consultar los padecimientos
        const conn = await context.db("nutricion");

        if (args.idPaciente) {
            /**
             * Asegura que todos los padecimientos tengan una relacion
             * con este paciente
             */
            const [counters] = await conn.execute("sql@conteoPadecimiento", [
                args.idPaciente,
            ]);
            counters.forEach(async counter => {
                if (counter.cantidad == "0") {
                    await conn.execute("sql@insertRelacion", [
                        counter.idPadecimiento, args.idPaciente,
                    ]);
                }
            });

            const [padecimientos]
                = await conn.execute("sql@padecimientoPaciente", [
                    args.idPaciente,
                ]);
            return padecimientos.map(padecimiento => {
                padecimiento.activo
                    = padecimiento.activo === 1;
                return padecimiento;
            });
        } else {
            const [padecimientos] = await conn.execute("sql@padecimientos");
            return padecimientos;
        }
    },
};
