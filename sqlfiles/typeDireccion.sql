
-- @pais
SELECT
	pais.id,
    pais.nombre
FROM
	paises pais
JOIN
	entidades_federativas entidad ON (
		entidad.idPais = pais.id
    )
JOIN
	municipios municipio ON (
		municipio.idEntidadFederativa = entidad.id
    )
JOIN
	colonias colonia ON (
		colonia.idMunicipio = municipio.id
    )
WHERE
	colonia.id = ?
-- @

-- @entidadFederativa
SELECT
	entidad.id,
    entidad.nombre
FROM
	entidades_federativas entidad
JOIN
	municipios municipio ON (
		municipio.idEntidadFederativa = entidad.id
    )
JOIN
	colonias colonia ON (
		colonia.idMunicipio = municipio.id
    )
WHERE
	colonia.id = ?
-- @

-- @municipio
SELECT
	municipio.id,
    municipio.nombre
FROM
	municipios municipio
JOIN
	colonias colonia ON (
		colonia.idMunicipio = municipio.id
    )
WHERE
	colonia.id = ?
-- @

-- @colonia
SELECT
	colonia.id,
    colonia.nombre
FROM
	colonias colonia
WHERE
	colonia.id = ?
-- @
