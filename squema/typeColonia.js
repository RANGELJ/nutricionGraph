
module.exports = new GraphQLObjectType({
    name: "typeColonia",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
    }),
});
