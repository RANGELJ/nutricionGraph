
-- @contador
SELECT
	alimento.id AS idAlimento,
    (SELECT COUNT(*) FROM alimento_paciente
		WHERE idPaciente = ?
        AND idAlimento = alimento.id) AS cantidad
FROM
	alimentos alimento
-- @

-- @insertRelacion
INSERT INTO alimento_paciente
    (idAlimento, idPaciente, activo)
    VALUES
    (?, ?, 1)
-- @

-- @elementos
SELECT
	relacion.id,
    alimento.nombre,
    relacion.idFrecuencia,
    relacion.activo
FROM
	alimento_paciente relacion
JOIN
	alimentos alimento ON (
		alimento.id = relacion.idAlimento
    )
WHERE
	relacion.idPaciente = ?
    AND alimento.idCategoria = ?
-- @
