module.exports = {
    type: "@Mutation",
    sqlfiles: ["updateProblemaActual"],
    args: {
        id: {type: "@NonNull(Int)"},
        otros: {type: "@NonNull(String)"},
        observaciones: {type: "@NonNull(String)"},
        enfermedadesDiagnosticadas: {type: "@NonNull(String)"},
        enfermedadesImportantes: {type: "@NonNull(String)"},
    },
    async resolve(root, args, context) {
        // @Error al actualizar la descripcion de los problemas
        const conn = await context.db("nutricion");
        await conn.execute("sql@updateProblemas", [
            args.otros, args.observaciones, args.enfermedadesDiagnosticadas,
            args.enfermedadesImportantes, args.id,
        ]);
        return {id: args.id};
    },
};
