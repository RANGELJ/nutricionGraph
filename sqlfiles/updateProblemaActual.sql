
-- @updateProblemas
UPDATE problemas_actuales
	SET
		otros = ?,
        observaciones = ?,
        enfermedadesDiagnosticadas = ?,
        enfermedadesImportantes = ?
WHERE
	id = ?
-- @