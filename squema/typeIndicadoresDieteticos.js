module.exports = new GraphQLObjectType({
    name: "typeIndicadoresDieteticos",
    fields: () => ({
        id: {type: "@Int"},
        comidasAlDia: {type: "@Int"},
        idPreparaAlimentos: {type: "@Int"},
        idTipoApetito: {type: "@Int"},
        idHoraMasHambre: {type: "@Int"},
        cantidadVasosAguaDia: {type: "@Int"},
        cantidadVasosBebidas: {type: "@Int"},
    }),
});
