module.exports = {
    type: "@Mutation",
    sqlfiles: ["updateIndicadoresDieteticos"],
    args: {
        idPaciente: {type: "@NonNull(Int)"},
        comidasAlDia: {type: "@NonNull(Int)"},
        idPreparaAlimentos: {type: "@NonNull(Int)"},
        idTipoApetito: {type: "@NonNull(Int)"},
        idHoraMasHambre: {type: "@NonNull(Int)"},
        cantidadVasosAguaDia: {type: "@NonNull(Int)"},
        cantidadVasosBebidas: {type: "@NonNull(Int)"},
    },
    async resolve(root, args, context) {
        // @Error al actualizar los indicadores dieteticos
        const conn = await context.db("nutricion");
        await conn.execute("sql@updateIndicadores", [
            args.comidasAlDia, args.idPreparaAlimentos,
            args.idTipoApetito, args.idHoraMasHambre,
            args.cantidadVasosAguaDia, args.cantidadVasosBebidas,
            args.idPaciente,
        ]);
        return {id: args.idPaciente};
    },
};
