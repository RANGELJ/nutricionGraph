module.exports = new GraphQLObjectType({
    name: "typeMunicipio",
    sqlfiles: ["typeMunicipio"],
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        colonias: {
            type: "@List(Colonia)",
            async resolve(municipio, args, context) {
                // @Error al consultar las colonias
                const conn = await context.db("nutricion");
                const params = [municipio.id];
                const [colonias] = await conn.execute("sql@colonias", params);
                conn.destroy();
                return colonias;
            },
        },
    }),
});
