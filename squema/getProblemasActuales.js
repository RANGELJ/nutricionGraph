module.exports = {
    type: "@ProblemaActual",
    args: {
        idPaciente: {type: "@NonNull(Int)"},
    },
    sqlfiles: ["getProblemasActuales"],
    async resolve(root, args, context) {
        // @Error al consultar los problemas actuales
        const conn = await context.db("nutricion");
        let [problemas] = await conn.execute("sql@problemasActuales", [
            args.idPaciente,
        ]);
        if (problemas.length < 1) {
            await conn.execute("sql@nuevoProblemas", [
                args.idPaciente,
            ]);
            [problemas] = await conn.execute("sql@problemasActuales", [
                args.idPaciente,
            ]);
        }
        conn.destroy();
        return problemas[0];
    },
};
