
-- @contador
SELECT
	medicamento.id AS idMedicamento,
    (SELECT COUNT(*) FROM medicamento_paciente
		WHERE idPaciente = ?
        AND idMedicamento = medicamento.id) AS cantidad
FROM
	medicamentos medicamento
-- @contador

-- @insertRelacion
INSERT INTO medicamento_paciente
    (idMedicamento, idPaciente, activo)
    VALUES
    (?, ?, b'0')
-- @

-- @medicamentos
SELECT
	id,
    nombre
FROM
	medicamentos
WHERE
	activo = 1
ORDER BY nombre
-- @

-- @medicamentosPaciente
SELECT
	relacion.id,
    medicamento.nombre,
    relacion.activo
FROM
	medicamento_paciente relacion
JOIN
	medicamentos medicamento ON (
		medicamento.id = relacion.idMedicamento
    )
WHERE
	relacion.idPaciente = ?
-- @
