module.exports = new GraphQLObjectType({
    name: "BooleanFlag",
    fields: () => ({
        flag: {type: "@Boolean"},
    }),
});
