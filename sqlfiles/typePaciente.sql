-- @estadoCivil
SELECT
    estadoCivil.id,
    estadoCivil.descripcion AS nombre
FROM
    estados_civiles estadoCivil
WHERE
    estadoCivil.activo = 1
    AND estadoCivil.id = ?
-- @

-- @direccion
SELECT
    id,
	calle,
    referencias,
    numero,
    idColonia
FROM
	direcciones
WHERE
	id = ?
-- @

-- @intensidadFisica
SELECT
	id,
    nombre
FROM
	intensidades_actividad_fisica
WHERE
	id = ?
-- @

-- @frecuenciaEjercicio
SELECT
	id,
	nombre
FROM
	frecuencias_ejercicio
WHERE
	id = ?
-- @

-- @duracion
SELECT
	id,
    nombre
FROM
	duraciones_ejercicio
WHERE
	id = ?
-- @
