module.exports = new GraphQLObjectType({
    name: "mutationResult",
    fields: () => ({
        id: {type: "@Int"},
    }),
});
