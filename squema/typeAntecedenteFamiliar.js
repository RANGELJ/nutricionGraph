module.exports = new GraphQLObjectType({
    name: "typeAntecedenteFamiliar",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        activo: {type: "@Boolean"},
    }),
});
