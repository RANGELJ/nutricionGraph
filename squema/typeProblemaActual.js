module.exports = new GraphQLObjectType({
    name: "typeProblemaActual",
    fields: () => ({
        id: {type: "@Int"},
        otros: {type: "@String"},
        observaciones: {type: "@String"},
        enfermedadesDiagnosticadas: {type: "@String"},
        enfermedadesImportantes: {type: "@String"},
    }),
});
