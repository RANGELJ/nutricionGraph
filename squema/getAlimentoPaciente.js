module.exports = {
    type: "@List(AlimentoPaciente)",
    sqlfiles: ["getAlimentoPaciente"],
    args: {
        idPaciente: {type: "@NonNull(Int)"},
        idCategoria: {type: "@NonNull(Int)"},
    },
    async resolve(root, args, context) {
        // @Error al consultar los alimentos por paciente
        const conn = await context.db("nutricion");

        const [alimentos] = await conn.execute("sql@elementos", [
            args.idPaciente, args.idCategoria,
        ]);

        return alimentos.map(alimentos => {
            alimentos.activo
                = alimentos.activo === 1;
            return alimentos;
        });
    },
};
