
-- @reporte
SELECT
	padecimiento.nombre,
    (
		SELECT
			COUNT(relacion.id)
		FROM
			padecimiento_paciente relacion
		JOIN
			pacientes paciente ON (
				paciente.id = relacion.idPaciente
			)
		JOIN
			generales ON (
				generales.id = paciente.idGenerales
			)
		WHERE
			generales.sexo = 'F'
			AND paciente.activo = 1
			AND relacion.activo = 1
			AND relacion.idPadecimiento = padecimiento.id
    ) AS totalMujeres,
    (
		SELECT
			COUNT(relacion.id)
		FROM
			padecimiento_paciente relacion
		JOIN
			pacientes paciente ON (
				paciente.id = relacion.idPaciente
			)
		JOIN
			generales ON (
				generales.id = paciente.idGenerales
			)
		WHERE
			generales.sexo = 'M'
			AND relacion.activo = 1
			AND relacion.idPadecimiento = padecimiento.id
    ) AS totalHombres
FROM
	padecimientos padecimiento
WHERE
	padecimiento.activo = 1
ORDER BY padecimiento.nombre
-- @
