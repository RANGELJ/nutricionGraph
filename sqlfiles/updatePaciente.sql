
-- @getIds
SELECT
    generales.id AS idGenerales,
    generales.idDireccion
FROM
    pacientes paciente
JOIN
    generales ON (
        generales.id = paciente.idGenerales
    )
WHERE
    paciente.id = ?
-- @

-- @updateGenerales
UPDATE generales
    SET
        nombre = ?,
        apellidoPaterno = ?,
        apellidoMaterno = ?,
        fechaNacimiento = ?,
        telefono = ?,
        idEstadoCivil = ?,
        sexo = ?
    WHERE
        id = ?
-- @

-- @updateDireccion
UPDATE direcciones
    SET
        idColonia = ?,
        calle = ?,
        referencias = ?,
        numero = ?
    WHERE
        id = ?
-- @
