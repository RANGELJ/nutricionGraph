module.exports = {
    type: "@Mutation",
    sqlfiles: ["updateAlimentoPaciente"],
    args: {
        id: {type: "@NonNull(Int)"},
        idFrecuencia: {type: "@NonNull(Int)"},
    },
    async resolve(root, args, context) {
        // @Error al actualizar los alimentos
        const conn = await context.db("nutricion");
        await conn.execute("sql@update", [
            args.idFrecuencia, args.id,
        ]);
        return {id: args.id};
    },
};
