
-- @entidades
SELECT
	id,
    nombre
FROM
	entidades_federativas
WHERE
	activo = 1
    AND idPais = ?
ORDER BY nombre
-- @
