module.exports = {
    type: "@List(Paciente)",
    args: {
        id: {type: "@Int"},
    },
    sqlfiles: ["getPacientes"],
    async resolve(root, args, context) {
        // @Error al consultar los pacientes
        const conn = await context.db("nutricion");
        const [pacientes]
            = await conn.executeWithOptionalParams("sql@pacientes", {
                optionalParams: [
                    [args.id, " AND paciente.id = ? "],
                ],
                endSQL: " ORDER BY generales.nombre ",
            });
        conn.destroy();
        return pacientes;
    },
};
