module.exports = {
    type: "@Mutation",
    sqlfiles: ["deletePacinete"],
    args: {
        id: {type: "@NonNull(Int)"},
    },
    async resolve(root, args, context) {
        // @Error al intentar eliminar el paciente
        const conn = await context.db("nutricion");
        await conn.execute("sql@deletePaciente", [args.id]);
        return {id: args.id};
    },
};
