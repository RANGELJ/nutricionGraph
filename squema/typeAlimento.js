module.exports = new GraphQLObjectType({
    name: "typeAlimento",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        activo: {type: "@Boolean"},
    }),
});
