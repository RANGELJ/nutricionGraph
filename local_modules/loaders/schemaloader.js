
const fs = require("fs");

module.exports = function(zrootsource, map, meta) {
    const callback = this.async();
    fs.readdir(__dirname + "/../../squema", (error, files) => {
        if (error) {
            callback(error);
        } else {
            let queryFields = "";
            let mutationFields = "";
            files.forEach(fileName => {
                if (fileName.endsWith(".js")) {
                    fieldName = fileName.split(".")[0];
                    if (fileName.startsWith("get")
                        || fileName.startsWith("is")) {
                        queryFields
                            += `${fieldName}: require("./${fileName}"),\n`;
                    } else if (fileName.startsWith("delete")
                        || fileName.startsWith("add")
                        || fileName.startsWith("update")
                        || fileName.startsWith("do") ) {
                        mutationFields
                            += `${fieldName}: require("./${fileName}"),\n`;
                    }
                }
            });
            zrootsource = zrootsource
                .replace("// All query fields (Do not remove comment as it"
                    + " is used by loader)", queryFields);
            zrootsource = zrootsource
                .replace("// All mutation field (Do not remove comment as "
                    + "it is used by loader)", mutationFields);
            callback(null, zrootsource, map, meta);
        }
    });
};
