module.exports = {
    type: "@List(CategoriaAlimento)",
    sqlfiles: ["getCategoriasAlimentos"],
    async resolve(root, args, context) {
        // @Error al consultar las categorias
        const conn = await context.db("nutricion");
        const [categorias] = await conn.execute("sql@categorias");
        return categorias;
    },
};
