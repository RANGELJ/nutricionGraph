
module.exports = {
    connections: [
        {
            name: "nutricion",
            config: {
                connectionLimit: 1100,
                host: "localhost",
                port: 3306,
                user: "nutricion_user",
                password: "t5E42w7PE%@6ZAtketTS",
                database: "nutricion",
                dateStrings: "date",
            },
        },
    ],
    sessionConfig: {
        connectionLimit: 2,
        host: "localhost",
        port: 3306,
        user: "nutricion_user",
        password: "t5E42w7PE%@6ZAtketTS",
        database: "nutricion",
        // The maximum age of a valid session; milliseconds:
        maxAgeMilliseconds: 86400000,
    },
};
