
-- @addDireccion
INSERT INTO direcciones
    (idColonia,calle,numero,referencias)
    VALUES (?,?,?,?)
-- @

-- @addGenerales
INSERT INTO generales
    (
        nombre,apellidoPaterno,
        apellidoMaterno,telefono,idEstadoCivil,
        sexo,idDireccion,fechaNacimiento
    )
    VALUES (?, ?, ?, ?, ?, ?, ?, ?)
-- @

-- @addPaciente
INSERT INTO pacientes
    (idGenerales,activo,momentoRegistro)
    VALUE
    (?,b'1',NOW())
-- @
