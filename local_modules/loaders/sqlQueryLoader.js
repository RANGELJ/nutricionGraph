
const rfs = require("../rfs");

const sqlDirectory = __dirname + "/../../sqlfiles/";

const getQueriesFromSource = async function(rawData) {
    const queryNameReguex = /-- @[a-zA-Z][a-zA-Z0-9]*/;
    const querySegment = /-- @[a-zA-Z][a-zA-Z0-9]*?[^\-]+-- @/g;
    const namedQueries = rawData.match(querySegment);

    const queries = {};

    namedQueries.forEach(namedQuery => {
        let queryName = namedQuery.match(queryNameReguex)[0];
        queryName = queryName.replace("-- @", "");
        let queryBody = namedQuery.replace(queryNameReguex, "");
        queryBody = queryBody.replace("-- @", "");

        queries[queryName] = queryBody.replace(/\n/g, " ")
            .replace(/\s+/g, " ");
    });

    return queries;
};

const loadQueryStrings = async (sourceCode) => {
    const variableName = "[a-zA-Z][a-zA-Z0-9]*";
    const fileReference = `^(${variableName}:)?${variableName}$`;
    const fileAttribute = `sqlfiles\\s*:\\s*\\[[^\\[]*\\]\\s*,\\s*`;

    const fileAttributeMatches
        = sourceCode.match(new RegExp(fileAttribute, "g"));

    if (fileAttributeMatches === null) {
        return [];
    } else if (fileAttributeMatches.length > 1) {
        throw "No debe haber mas de un atributo sqlfile como referencia";
    }

    const fileAtributesString = fileAttributeMatches[0];
    if (! fileAttributeMatches) {
        return undefined;
    }

    const fileAtributesStringList = fileAtributesString
        .replace(/^sqlfiles\s*:\s*\[/, "")
        .replace(/\s*\]\s*,\s*$/);

    const references =
        fileAtributesStringList.match(/"[^"]*"/g)
            .map(reference => reference.replace(/"/g, ""));

    let queryReferences = {};

    const fileReferenceRegex = new RegExp(fileReference);

    for (let i = 0; i < references.length; i++) {
        const reference = references[i];
        if (! fileReferenceRegex.test(reference)) {
            throw `La referenca a archivo sql "${reference}" no es valida`;
        }
        const referenceParts = reference.split(":");
        let fileName = "";
        let alias = "";
        if (referenceParts.length === 1) {
            fileName = referenceParts[0];
            alias = fileName;
        } else {
            fileName = referenceParts[1];
            alias = referenceParts[0];
        }
        const filePath = sqlDirectory + fileName + ".sql";
        let queryFileString = await rfs.readFile(filePath, "utf8");

        const queriesObject = await getQueriesFromSource(queryFileString);
        queryReferences[alias] = queriesObject;
    }

    return queryReferences;
};

const loadQueryReferences = async (sourceCode) => {
    const variableName = "[a-zA-Z][a-zA-Z0-9]*";
    const queryReferencePatern = `sql@(${variableName}?\\/)?${variableName}`;

    const queryReferences
        = sourceCode.match(new RegExp(queryReferencePatern, "g"));

    if (queryReferences === null) {
        return undefined;
    }

    const references = [];
    queryReferences.forEach(queryReference => {
        const raw = queryReference;
        queryReference = queryReference.replace("sql@", "");
        const referenceComponents = queryReference.split("/");
        if (referenceComponents.length === 1) {
            references.push({
                name: referenceComponents[0],
                raw,
            });
        } else {
            references.push({
                name: referenceComponents[1],
                file: referenceComponents[0],
                raw,
            });
        }
    });

    return references;
};

const loadFunction = async (sourceCode) => {
    try {
        const queryReferences = await loadQueryReferences(sourceCode);

        if (queryReferences !== undefined) {
            const queries = await loadQueryStrings(sourceCode);

            let newSource = sourceCode;

            if (queries === undefined || queries.length < 1) {
                throw "No se encontro referencia a archivos sql, "
                    + "y el archivo tiene queries";
            }

            queryReferences.forEach(queryReference => {
                if (queryReference.file) {
                    const queryFile = queries[queryReference.file];
                    if (queryFile === undefined) {
                        throw "Archivo sql en referencia no encontrado: "
                            + queryReference.raw;
                    }
                    const query = queryFile[queryReference.name];
                    if (query === undefined) {
                        throw "No se encontro el query con nombre: "
                            + queryReference.name;
                    }

                    newSource = newSource.split(queryReference.raw).join(query);
                } else {
                    let query = undefined;
                    Object.keys(queries).forEach(fileName => {
                        Object.keys(queries[fileName]).forEach(queryName => {
                            if (queryName === queryReference.name) {
                                if (query === undefined) {
                                    query = queries[fileName][queryName];
                                } else {
                                    throw "El nombre del query: " + queryName
                                        + " es ambiguo en : " + fileName;
                                }
                            }
                        });
                    });
                    newSource = newSource.split(queryReference.raw).join(query);
                }
            });

            return newSource.replace(/sqlfiles\s*:\s*\[[^\[]*\]\s*,\s*/g, "");
        } else {
            return sourceCode;
        }
    } catch (error) {
        throw "Error en ejecucion de sqlQueryLoader: " + error;
    }
};

module.exports = function(source, map, meta) {
    const callback = this.async();

    loadFunction(source).then(newSource => {
        callback(null, newSource, map, meta);
    }).catch(error => {
        callback({message: error}, source, map, meta);
    });
};

