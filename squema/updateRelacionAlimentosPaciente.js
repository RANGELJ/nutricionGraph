module.exports = {
    type: "@Mutation",
    sqlfiles: ["updateRelacionAlimentosPaciente"],
    async resolve(root, args, context) {
        // @Error al actualizar la relacion de alimentos paciente
        const conn = await context.db("nutricion");

        const [pacientes] = await conn.execute("sql@pacientes");

        for (let ip = 0; ip < pacientes.length; ip ++) {
            const paciente = pacientes[ip];

            const [counters] = await conn.execute("sql@contador", [
                paciente.id,
            ]);

            for (let i = 0; i < counters.length; i++) {
                const counter = counters[i];
                if (counter.cantidad == "0") {
                    await conn.execute("sql@insertRelacion", [
                        counter.idAlimento, paciente.id,
                    ]);
                }
            }
        }

        conn.destroy();
        return {id: 1};
    },
};
