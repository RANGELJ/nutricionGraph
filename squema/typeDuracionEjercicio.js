module.exports = new GraphQLObjectType({
    name: "typeDuracionEjercicio",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
    }),
});
