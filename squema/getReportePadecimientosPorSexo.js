module.exports = {
    type: "@List(ReportePadecimientosPorSexo)",
    sqlfiles: ["getReportePadecimientosPorSexo"],
    async resolve(root, args, context) {
        // @Error al consultar los datos del reporte
        const conn = await context.db("nutricion");
        let [registros] = await conn.execute("sql@reporte");
        conn.destroy();
        return registros;
    },
};
