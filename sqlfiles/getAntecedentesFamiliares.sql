
-- @contador
SELECT
	antecedente.id AS idAntecedente,
    (SELECT COUNT(*) FROM antecendente_familiar_paciente
		WHERE idPaciente = ?
        AND idAntecedente = antecedente.id) AS cantidad
FROM
	antecedentes_familiares antecedente
-- @

-- @insertRelacion
INSERT INTO antecendente_familiar_paciente
    (idAntecedente, idPaciente, activo)
    VALUES
    (?, ?, b'0')
-- @

-- @antecedentes
SELECT
	id,
    nombre
FROM
	antecedentes_familiares
WHERE
    activo = 1
ORDER BY nombre
-- @

-- @antecedentePaciente
SELECT
	relacion.id,
    antecedente.nombre,
    relacion.activo
FROM
	antecendente_familiar_paciente relacion
JOIN
	antecedentes_familiares antecedente ON (
		antecedente.id = relacion.idAntecedente
    )
WHERE
	relacion.idPaciente = ?
-- @
