
-- @estadosCiviles
SELECT
    estadoCivil.id,
    estadoCivil.descripcion AS nombre
FROM
    estados_civiles estadoCivil
WHERE
    estadoCivil.activo = 1
-- @