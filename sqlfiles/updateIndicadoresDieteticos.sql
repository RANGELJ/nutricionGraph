
-- @updateIndicadores
UPDATE indicadores_dieteticos SET
	comidasAlDia = ?,
    idPreparaAlimentos = ?,
    idTipoApetito = ?,
    idHoraMasHambre = ?,
    cantidadVasosAguaDia = ?,
    cantidadVasosBebidas = ?
WHERE
	idPaciente = ?
-- @
