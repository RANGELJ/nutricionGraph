module.exports = new GraphQLObjectType({
    name: "typeEstadoCivil",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
    }),
});
