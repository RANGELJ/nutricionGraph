module.exports = async function(root, args, context) {
    const nombreRegex = /^[A-Z][a-zA-Z\s^\n]{1,45}$/;

    if (! nombreRegex.test(args.nombre) ) {
        context.throwError("Nombre invalido");
    }

    if (! nombreRegex.test(args.apellidoPaterno)) {
        context.throwError("Apellido paterno invalido");
    }

    if (! nombreRegex.test(args.apellidoMaterno)) {
        context.throwError("Apellido materno invalido");
    }

    if (! /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(args.fechaNacimiento) ) {
        context.throwError("Fecha de nacimiento invalida");
    }

    if (! /^[0-9]{10}$/.test(args.telefono) ) {
        context.throwError("Telefono invalido");
    }

    if (["M", "F"].indexOf(args.sexo) === -1) {
        context.throwError("Sexo invalido");
    }

    if (! /^[a-zA-Z0-9][a-zA-Z0-9\s]{4,29}$/.test(args.calle) ) {
        context.throwError("Calle con formato invalido");
    }

    if (! /^[a-zA-Z0-9/]{1,10}$/.test(args.numeroCalle)) {
        context.throwError("Numero de calle invalido");
    }

    if (! /^[a-zA-Z0-9\s]{0,45}$/.test(args.referencias)) {
        context.throwError("Referencias con formato invalido");
    }

    return true;
};
