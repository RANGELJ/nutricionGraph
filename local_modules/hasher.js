
const crypto = require("crypto");

const hashBytes = 64;
const saltRange = [25, 35];
const iterations = 156899;

const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

const generateSalt = function() {
    return new Promise((resolve, reject) => {
        const saltBytes = getRandomInt(
            saltRange[0], saltRange[1]
        );
        crypto.randomBytes(saltBytes, (error, salt) => {
            if (error) {
                reject(error);
            } else {
                resolve(salt);
            }
        });
    });
};

const hashSaltedPassword = function(password, salt) {
    return new Promise((resolve, reject) => {
        crypto.pbkdf2(password, salt,
            iterations, hashBytes, "RSA-SHA512",
            (error, hash) => {
                if (error) {
                    reject(error);
                } else {
                    resolve({hash, salt});
                }
            });
    });
};

const hash = function(password) {
    return new Promise((resolve, reject) => {
        generateSalt()
            .then(salt => hashSaltedPassword(password, salt))
            .then(({hash, salt}) => {
                resolve({
                    hash: hash.toString("hex"),
                    salt: salt.toString("hex"),
                });
            })
            .catch(error => reject(error));
    });
};

const verify = function(password, saltIn, hashIn) {
    return new Promise((resolve, reject) => {
        hashSaltedPassword(password, saltIn)
            .then(({hash, salt}) => {
                resolve(
                    hash.toString("hex") === hashIn
                );
            })
            .catch(error => reject(error));
    });
};

module.exports = {hash, verify};

/* hash("159263")
    .then(({hash, salt})=>verify("159263", salt, hash))
    .then(res => console.log(res))
    .catch(error => console.log(error));*/
