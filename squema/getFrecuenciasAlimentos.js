module.exports = {
    type: "@List(FrecuenciaAlimento)",
    sqlfiles: ["getFrecuenciasAlimentos"],
    async resolve(root, args, context) {
        // @Error al consultar las frecuencias de alimentos
        const conn = await context.db("nutricion");
        const [frecuencias] = await conn.execute("sql@frecuencias");
        return frecuencias;
    },
};
