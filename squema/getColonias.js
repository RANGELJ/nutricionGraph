
module.exports = {
    type: "@List(Colonia)",
    sqlfiles: ["getColonias"],
    args: {
        idMunicipio: {type: "@Int"},
    },
    async resolve(root, args, context) {
        // @Error al consultar las colonias
        const conn = await context.db("nutricion");
        const [rows] = await conn.executeWithOptionalParams("sql@colonias", {
            optionalParams: [
                [args.idMunicipio, " AND colonia.idMunicipio = ? "],
            ],
            endSQL: " ORDER BY colonia.nombre LIMIT 9000 ",
        });
        return rows;
    },
};
