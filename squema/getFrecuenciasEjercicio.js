module.exports = {
    type: "@List(FrecuenciaEjercicio)",
    sqlfiles: ["getFrecuenciasEjercicio"],
    async resolve(root, args, context) {
        // @Error al consultar las frecuencias de ejercicio
        const conn = await context.db("nutricion");
        const [frecuencias] = await conn.execute("sql@frecuencias");
        return frecuencias;
    },
};
