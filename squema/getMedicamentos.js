module.exports = {
    type: "@List(Medicamento)",
    args: {
        idPaciente: {type: "@Int"},
    },
    sqlfiles: ["getMedicamentos"],
    async resolve(root, args, context) {
        // @Error al consultar los medicamentos
        const conn = await context.db("nutricion");

        if (args.idPaciente) {
            const [counters] = await conn.execute("sql@contador", [
                args.idPaciente,
            ]);
            counters.forEach(async counter => {
                if (counter.cantidad == "0") {
                    await conn.execute("sql@insertRelacion", [
                        counter.idMedicamento, args.idPaciente,
                    ]);
                }
            });

            const [medicamentos]
                = await conn.execute("sql@medicamentosPaciente", [
                    args.idPaciente,
                ]);
            return medicamentos.map(medicamento => {
                medicamento.activo
                    = medicamento.activo === 1;
                return medicamento;
            });
        } else {
            const [medicamentos] = await conn.execute("sql@medicamentos");
            return medicamentos;
        }
    },
};
