module.exports = new GraphQLObjectType({
    name: "typeReportePadecimientosPorSexo",
    fields: () => ({
        nombre: {type: "@String"},
        totalMujeres: {type: "@Int"},
        totalHombres: {type: "@Int"},
    }),
});
