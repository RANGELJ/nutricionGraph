module.exports = {
    type: "@List(Alimento)",
    sqlfiles: ["getAlimentos"],
    async resolve(root, args, context) {
        // @Error al consultar los alimentos
        const conn = await context.db("nutricion");
        const [alimentos] = await conn.execute("sql@alimentos");
        return alimentos;
    },
};
