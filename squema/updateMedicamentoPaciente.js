const {
    GraphQLList,
    GraphQLInt,
    GraphQLBoolean,
    GraphQLNonNull,
} = require("graphql");

module.exports = {
    type: new GraphQLList(require("./typeMutation")),
    sqlfiles: ["updateMedicamentoPaciente"],
    args: {
        ids: {type: new GraphQLNonNull( new GraphQLList(GraphQLInt) )},
        activos: {type: new GraphQLNonNull( new GraphQLList(GraphQLBoolean) )},
    },
    async resolve(root, args, context) {
        // @Error al actualizar los medicamentos
        if (args.ids.length !== args.activos.length) {
            context.throwError("Debes indicar la misma cantidad de ids"
                + " y activos");
        }
        const conn = await context.db("nutricion");
        for (let i = 0; i < args.ids.length; i++) {
            const id = args.ids[i];
            const activo = args.activos[i] ? 1 : 0;
            await conn.execute("sql@updateRelacion", [
                activo, id,
            ]);
        }
        return [];
    },
};
