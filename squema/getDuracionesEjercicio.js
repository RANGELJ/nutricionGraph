module.exports = {
    type: "@List(DuracionEjercicio)",
    sqlfiles: ["getDuracionesEjercicio"],
    async resolve(root, args, context) {
        // @Error al consultar las duraciones
        const conn = await context.db("nutricion");
        const [duraciones] = await conn.execute("sql@duraciones");
        return duraciones;
    },
};
