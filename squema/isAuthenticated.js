module.exports = {
    type: "@BooleanFlag",
    async resolve(root, args, context) {
        // @Error al determinar si el usuario esta registrado
        return {
            flag: context.session.idUser !== undefined,
        };
    },
};
