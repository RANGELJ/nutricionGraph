module.exports = new GraphQLObjectType({
    name: "typeCategoriaAlimento",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
    }),
});
