
-- @colonias
SELECT
	id,
    nombre
FROM
	colonias
WHERE
	activo = 1
    AND idMunicipio = ?
ORDER BY nombre
-- @