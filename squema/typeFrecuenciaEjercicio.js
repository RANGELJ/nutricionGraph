module.exports = new GraphQLObjectType({
    name: "typeFrecuenciaEjercicio",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
    }),
});
