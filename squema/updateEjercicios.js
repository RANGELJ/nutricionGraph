module.exports = {
    type: "@Mutation",
    sqlfiles: ["updateEjercicio"],
    args: {
        idPaciente: {type: "@NonNull(Int)"},
        idIntencidadFisica: {type: "@NonNull(Int)"},
        idFrecuenciaEjercicio: {type: "@NonNull(Int)"},
        idDuracionEjercicio: {type: "@NonNull(Int)"},
        fechaInicioEjercicio: {type: "@NonNull(String)"},
        aspectoGeneral: {type: "@NonNull(String)"},
    },
    async resolve(root, args, context) {
        // @Error al registrar los datos del ejercicio del paciente
        const conn = await context.db("nutricion");
        await conn.execute("sql@updateEjercicio", [
            args.idIntencidadFisica, args.idFrecuenciaEjercicio,
            args.idDuracionEjercicio, args.fechaInicioEjercicio,
            args.aspectoGeneral, args.idPaciente,
        ]);
        return {id: args.idPaciente};
    },
};
