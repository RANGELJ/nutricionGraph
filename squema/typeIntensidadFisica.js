module.exports = new GraphQLObjectType({
    name: "typeIntensidadFisica",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
    }),
});
