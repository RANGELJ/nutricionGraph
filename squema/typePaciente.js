module.exports = new GraphQLObjectType({
    name: "typePacientes",
    sqlfiles: ["typePaciente"],
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        apellidoPaterno: {type: "@String"},
        apellidoMaterno: {type: "@String"},
        edad: {type: "@Int"},
        fechaNacimiento: {type: "@String"},
        telefono: {type: "@String"},
        sexo: {type: "@String"},
        fechaInicioEjercicio: {type: "@String"},
        aspectoGeneral: {type: "@String"},
        estadoCivil: {
            type: "@EstadoCivil",
            async resolve(paciente, args, context) {
                // @Error al consultar el estado civil del paciente
                const conn = await context.db("nutricion");
                const params = [paciente.idEstadoCivil];
                const [rows] = await conn.execute("sql@estadoCivil", params);
                return rows[0];
            },
        },
        direccion: {
            type: "@Direccion",
            async resolve(paciente, args, context) {
                // @Error al consultar la direccion
                const conn = await context.db("nutricion");
                const params = [paciente.idDireccion];
                const [rows] = await conn.execute("sql@direccion", params);
                return rows[0];
            },
        },
        instensidadFisica: {
            type: "@IntensidadFisica",
            async resolve(paciente, args, context) {
                // @Error al consultar la intensidad fisica
                const conn = await context.db("nutricion");
                const [intensidad]
                    = await conn.execute("sql@intensidadFisica", [
                        paciente.idIntencidadFisica,
                    ]);
                return intensidad[0];
            },
        },
        frecuenciaEjercicio: {
            type: "@FrecuenciaEjercicio",
            async resolve(paciente, args, context) {
                // @Error al consultar la frecuencia de ejercicio
                const conn = await context.db("nutricion");
                const [frecuencia]
                    = await conn.execute("sql@frecuenciaEjercicio", [
                        paciente.idFrecuenciaEjercicio,
                    ]);
                return frecuencia[0];
            },
        },
        duracionEjercicio: {
            type: "@DuracionEjercicio",
            async resolve(paciente, args, context) {
                // @Error al consultar la duracion del ejercicio del paciente
                const conn = await context.db("nutricion");
                const [duracion] = await conn.execute("sql@duracion", [
                    paciente.idDuracionEjercicio,
                ]);
                return duracion[0];
            },
        },
    }),
});
