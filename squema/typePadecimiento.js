module.exports = new GraphQLObjectType({
    name: "typePadecimiento",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        activo: {type: "@Boolean"},
    }),
});
