const validator = require("./validatorPaciente.gph");

module.exports = {
    type: "@Mutation",
    args: {
        nombre: {type: "@NonNull(String)"},
        apellidoPaterno: {type: "@NonNull(String)"},
        apellidoMaterno: {type: "@NonNull(String)"},
        fechaNacimiento: {type: "@NonNull(String)"},
        telefono: {type: "@NonNull(String)"},
        idEstadoCivil: {type: "@NonNull(Int)"},
        sexo: {type: "@NonNull(String)"},
        idColonia: {type: "@NonNull(Int)"},
        calle: {type: "@NonNull(String)"},
        numeroCalle: {type: "@NonNull(String)"},
        referencias: {type: "@NonNull(String)"},
    },
    sqlfiles: ["addPaciente"],
    async resolve(root, args, context) {
        // @Error al registrar el paciente
        await validator(root, args, context);
        const conn = await context.db("nutricion");
        const [metaD] = await conn.execute("sql@addDireccion", [
            args.idColonia, args.calle, args.numeroCalle, args.referencias,
        ]);
        const idDireccion = metaD.insertId;
        const [metaG] = await conn.execute("sql@addGenerales", [
            args.nombre, args.apellidoPaterno, args.apellidoMaterno,
            args.telefono, args.idEstadoCivil, args.sexo, idDireccion,
            args.fechaNacimiento,
        ]);
        const idGenerales = metaG.insertId;
        const [metaP] = await conn.execute("sql@addPaciente", [
            idGenerales,
        ]);
        return {id: metaP.insertId};
    },
};
