
module.exports = function(source, map, meta) {
    const callback = this.async();

    const resolveStartReguex
        = /async\s*resolve\s*\([^)]*\)\s*{/;

    let header = source.match(resolveStartReguex);

    let requiereWrapper = false;
    while (header !== null) {
        requiereWrapper = true;
        const headerText = header[0];
        const headerStart = header.index;
        const functionBodyStartIndex = header.index + headerText.length;

        const resolveParams = headerText.replace(/async\s*resolve\s*\(/g, "")
            .replace(/\)\s*{/g, "");

        // Count the number of brackets open
        let brackets = 1;
        let currIndex = functionBodyStartIndex;

        while (brackets > 0) {
            const currChar = source.charAt(currIndex);
            if (currChar === "{") {
                brackets++;
            } else if (currChar === "}") {
                brackets--;
            }
            currIndex++;
        }
        const functionEndIndex = currIndex;
        const functionBody
            = source.slice(functionBodyStartIndex, functionEndIndex-1);

        // Busca el comentario de error
        const errorReguex = /\/\/\s*@[^\n]*?\n/g;
        const errorMatches = functionBody.match(errorReguex);

        if (errorMatches === null || errorMatches.length < 1) {
            callback({
                message: "No se encontro la frase de error del resolve",
            }, source, map, meta);
            return;
        } else if (errorMatches.length > 1) {
            callback({
                message: "No debe haber mas de una frase de error por "
                    + "resolve function",
            }, source, map, meta);
            return;
        }
        const mensajeError = errorMatches[0]
            .replace(/\/\/\s*@/g, "").replace(/\n*/g, "");

        const newFunction = `resolve: resolveWrapper(async (${resolveParams}) => {
            ${functionBody.replace(errorReguex, "")}
        }, "${mensajeError}")`;

        source = source.slice(0, headerStart)
            + newFunction + source.slice(functionEndIndex, source.length);

        header = source.match(resolveStartReguex);
    }

    if (requiereWrapper) {
        source = `const {resolveWrapper} = require("./rgraph"); ` + source;
    }

    callback(null, source, map, meta);
};
