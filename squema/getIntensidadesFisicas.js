module.exports = {
    type: "@List(IntensidadFisica)",
    sqlfiles: ["getIntensidadesFisicas"],
    async resolve(root, args, context) {
        // @Error al consultar las intensidades fisicas
        const conn = await context.db("nutricion");
        const [intensidades] = await conn.execute("sql@intensidades");
        return intensidades;
    },
};
