const {GraphQLSchema, GraphQLObjectType} = require("graphql");

module.exports = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: "RootQuery",
        fields: {
            // All query fields (Do not remove comment as it is used by loader)
        },
    }),
    mutation: new GraphQLObjectType({
        name: "MutationQueries",
        fields: {
            // All mutation field (Do not remove comment as it is used by loader)
        }
    }),
});
