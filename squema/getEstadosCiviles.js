module.exports = {
    type: "@List(EstadoCivil)",
    sqlfiles: ["getEstadosCiviles"],
    async resolve(root, args, context) {
        // @Error al consultar los estados civiles
        const conn = await context.db("nutricion");
        const [rows] = await conn.execute("sql@estadosCiviles");
        return rows;
    },
};
