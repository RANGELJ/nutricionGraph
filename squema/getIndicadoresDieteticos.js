module.exports = {
    type: "@IndicadoresDieteticos",
    sqlfiles: ["getIndicadoresDieteticos"],
    args: {
        idPaciente: {type: "@NonNull(Int)"},
    },
    async resolve(root, args, context) {
        // @Error al consultar los indicadores dieteticos
        const conn = await context.db("nutricion");
        const [indicadores] = await conn.execute("sql@indicadores", [
            args.idPaciente,
        ]);
        if (indicadores.length < 1) {
            await conn.execute("sql@nuevoRegistro", [
                args.idPaciente,
            ]);
            const [indicadoresNuevos] = await conn.execute("sql@indicadores", [
                args.idPaciente,
            ]);
            conn.destroy();
            return indicadoresNuevos[0];
        } else {
            conn.destroy();
            return indicadores[0];
        }
    },
};
