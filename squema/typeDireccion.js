module.exports = new GraphQLObjectType({
    name: "typeDireccion",
    sqlfiles: ["typeDireccion"],
    fields: () => ({
        id: {type: "@Int"},
        calle: {type: "@String"},
        referencias: {type: "@String"},
        numero: {type: "@String"},
        pais: {
            type: "@Pais",
            async resolve(root, args, context) {
                // @Error al consultar el pais
                const conn = await context.db("nutricion");
                const params = [root.idColonia];
                const [rows] = await conn.execute("sql@pais", params);
                conn.destroy();
                return rows[0];
            },
        },
        entidadFederativa: {
            type: "@EntidadFederativa",
            async resolve(root, args, context) {
                // @Error al consultar la entidad federativa de la direccion
                const conn = await context.db("nutricion");
                const params = [root.idColonia];
                const [rows]
                    = await conn.execute("sql@entidadFederativa", params);
                conn.destroy();
                return rows[0];
            },
        },
        municipio: {
            type: "@Municipio",
            async resolve(root, args, context) {
                // @Error al consultar el municipio de la direccion
                const conn = await context.db("nutricion");
                const params = [root.idColonia];
                const [rows] = await conn.execute("sql@municipio", params);
                conn.destroy();
                return rows[0];
            },
        },
        colonia: {
            type: "@Colonia",
            async resolve(root, args, context) {
                // @Error al consultar la colonia de la direccion
                const conn = await context.db("nutricion");
                const params = [root.idColonia];
                const [rows] = await conn.execute("sql@colonia", params);
                conn.destroy();
                return rows[0];
            },
        },
    }),
});
