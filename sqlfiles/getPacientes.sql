
-- @pacientes
SELECT
    paciente.id,
    generales.idEstadoCivil,
    generales.idDireccion,
    generales.nombre,
    generales.apellidoPaterno,
    generales.apellidoMaterno,
    TIMESTAMPDIFF(YEAR,
        generales.fechaNacimiento,
        NOW()
    ) AS edad,
    generales.fechaNacimiento,
    generales.telefono,
    generales.sexo,
    paciente.idIntencidadFisica,
    paciente.idFrecuenciaEjercicio,
    paciente.idDuracionEjercicio,
    paciente.fechaInicioEjercicio,
    paciente.aspectoGeneral
FROM
    pacientes paciente
JOIN
    generales ON (
        generales.id = paciente.idGenerales
    )
WHERE
    paciente.activo = 1
-- @
