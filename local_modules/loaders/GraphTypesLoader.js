
const parseReference = function(reference) {
    const raw = reference[0];
    const startIndex = reference.index;
    const endIndex = startIndex + raw.length;

    const typeString = raw.replace(/type:\s*"@/g, "").replace(/"\s*/g, "");

    const rawComponent = typeString.split("(");
    const component = {startIndex, endIndex, raw};
    if (rawComponent.length === 2) {
        component.function = rawComponent[0];
        component.type = rawComponent[1].replace(")", "");
    } else {
        component.type = rawComponent[0];
    }
    return component;
};

const graphFunctions = ["NonNull", "List"];
const graphTypes = ["String", "Int", "Boolean", "Float"];
const processReference = function(reference) {
    const requiredGraphTypes =[];
    let procecedRef = "type: ";
    if (reference.function) {
        if (graphFunctions.indexOf(reference.function) === -1) {
            throw "Nombre de funcion invalida: " + reference.function;
        } else {
            const rawFunction = "GraphQL" + reference.function;
            requiredGraphTypes.push(rawFunction);
            procecedRef += "new "+rawFunction + "(";
        }
    }

    if (graphTypes.indexOf(reference.type) === -1) {
        procecedRef += `require("./type${reference.type}")`;
    } else {
        const rawType = "GraphQL" + reference.type;
        procecedRef += rawType;
        requiredGraphTypes.push(rawType);
    }

    if (reference.function) {
        procecedRef += ")";
    }

    return {procecedRef, requiredGraphTypes};
};

const identifierReguex = "[A-Z][A-Za-z0-9]*";
let referenceReguex
    = `type\\s*:\\s*"@${identifierReguex}`
        + `(\\(${identifierReguex}\\))?"`;
referenceReguex = new RegExp(referenceReguex);

const processSource = function(source) {
    const graphTypesSet = new Set();

    let matched = source.match(referenceReguex);
    while (matched !== null) {
        const reference = parseReference(matched);
        const {procecedRef, requiredGraphTypes} = processReference(reference);
        requiredGraphTypes
            .forEach(requiredType => graphTypesSet.add(requiredType));

        source = source.slice(0, reference.startIndex)
            + procecedRef
            + source.slice(reference.endIndex, source.length);

        matched = source.match(referenceReguex);
    }

    if (graphTypesSet.size > 0 || source.includes("GraphQLObjectType")) {
        let requireGraph = "const {";
        graphTypesSet.forEach(typeGraph => {
            requireGraph += typeGraph + ",";
        });
        if (source.includes("GraphQLObjectType")) {
            requireGraph += "GraphQLObjectType,";
        }
        requireGraph += "} = require(\"graphql\");";

        source = requireGraph + source;
    }
    return source;
};

module.exports = function(source, map, meta) {
    const callback = this.async();

    try {
        source = processSource(source);
        callback(null, source, map, meta);
    } catch (error) {
        callback({
            message: error,
        }, source, map, meta);
    }
};
