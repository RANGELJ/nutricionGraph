module.exports = {
    type: "@List(EntidadFederativa)",
    sqlfiles: ["getEntidadesFederativas"],
    args: {
        idPais: {type: "@Int"},
    },
    async resolve(root, args, context) {
        // @Error al consultar las entidades federativas
        const conn = await context.db("nutricion");
        const [rows] = await conn.executeWithOptionalParams("sql@entidades", {
            optionalParams: [
                [args.idPais, "AND entidad.idPais = ?"],
            ],
            endSQL: " ORDER BY entidad.nombre ",
        });
        return rows;
    },
};
