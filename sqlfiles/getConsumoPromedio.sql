
-- @promedioCats
SELECT
	SUM(relacion.idFrecuencia) AS sumaFrecuencias,
    (SELECT
		COUNT(*) FROM alimento_paciente alpac
        JOIN alimentos alr ON alr.id = alpac.idAlimento
        WHERE alpac.activo = 1 AND alpac.idFrecuencia IS NOT NULL
			AND  alr.idCategoria = categoria.id
	) AS totalElementos,
    categoria.nombre
FROM alimento_paciente relacion
JOIN
	alimentos alimento ON (
		alimento.id = relacion.idAlimento
    )
JOIN
	categorias_alimentos categoria ON (
		alimento.idCategoria = categoria.id
    )
WHERE
	relacion.activo = 1
    AND relacion.idFrecuencia IS NOT NULL
GROUP BY alimento.idCategoria
-- @