const webpack = require("webpack");
const nodeExternals = require("webpack-node-externals");
const path = require("path");

const nodeEnv = process.env.NODE_ENV;
const isProduction = nodeEnv !== "development";

let plugins = [
    new webpack.DefinePlugin({
        "process.env": {
            NODE_ENV: JSON.stringify(nodeEnv),
        },
    }),
    new webpack.NamedModulesPlugin()
];
 
if (!isProduction) {
    plugins.push(new webpack.HotModuleReplacementPlugin())
}

const entry = isProduction ? [
    "babel-polyfill",
    path.resolve(path.join(__dirname, "./index.js"))
] : [
    "babel-polyfill",
    path.resolve(path.join(__dirname, "./index.js"))
];

module.exports = {
    mode: "production", /* production development */
    devtool: false,
    externals: [
        nodeExternals()
    ],
    resolveLoader: {
        alias: {
            "schema_loader": path.join(__dirname, "local_modules", "loaders", "schemaloader.js"),
            "sql_query_loader": path.join(__dirname, "local_modules", "loaders", "sqlQueryLoader.js"),
            "resolve_function_loader": path.join(__dirname, "local_modules", "loaders", "resolveFunctionLoader.js"),
            "graph_types_loader": path.join(__dirname, "local_modules", "loaders", "GraphTypesLoader.js"),
        }
    },
    name: "server",
    target: "node",
    plugins: plugins,
    entry: entry,
    output: {
        publicPath: "./",
        path: path.resolve(__dirname, "./"),
        filename: "index.prod.js",
        libraryTarget: "commonjs2"
    },
    resolve: {
        extensions: [
            ".webpack-loader.js",
            ".web-loader.js",
            ".loader.js",
            ".js",
            ".jsx",
        ],
        modules: [
            path.resolve(__dirname, "node_modules")
        ]
    },
    module : {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                options : {
                    babelrc : true
                }
            },
            {
                test: /squema\/[^]+?\.js$/,
                exclude: /zroot\.js$/,
                loader: [
                    "graph_types_loader",
                    "resolve_function_loader",
                    "sql_query_loader",
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
                options: {
                    parser: "babel-eslint",
                }
            },
            {
                test: /zroot\.js$/,
                loader: "schema_loader"
            },
        ],
    },
    node: {
        console: false,
        global: false,
        process: false,
        Buffer: false,
        __filename: false,
        __dirname: false,
    }
};