module.exports = {
    type: "@List(Municipio)",
    args: {
        idEntidadFederativa: {type: "@Int"},
    },
    sqlfiles: ["getMunicipios"],
    async resolve(root, args, context) {
        // @Error al consultar los municipios
        const conn = await context.db("nutricion");
        const [rows] = await conn.executeWithOptionalParams("sql@municipios", {
            optionalParams: [
                [args.idEntidadFederativa,
                    " AND municipio.idEntidadFederativa = ? "],
            ],
            endSQL: " ORDER BY municipio.nombre ",
        });
        return rows;
    },
};
