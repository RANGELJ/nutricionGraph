
-- @problemasActuales
SELECT
	id,
    otros,
    observaciones,
    enfermedadesDiagnosticadas,
    enfermedadesImportantes
FROM
	problemas_actuales
WHERE
	activo = 1
    AND idPaciente = ?
-- @

-- @nuevoProblemas
INSERT INTO problemas_actuales
    (otros, observaciones, enfermedadesDiagnosticadas,
        enfermedadesImportantes, idPaciente, activo)
    VALUES
    ('','','','',?,1)
-- @