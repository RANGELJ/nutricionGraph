module.exports = new GraphQLObjectType({
    name: "typeAlimentoPaciente",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        activo: {type: "@Boolean"},
        idFrecuencia: {type: "@Int"},
    }),
});
