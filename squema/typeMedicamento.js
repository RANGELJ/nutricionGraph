module.exports = new GraphQLObjectType({
    name: "typeMedicamento",
    fields: () => ({
        id: {type: "@Int"},
        nombre: {type: "@String"},
        activo: {type: "@Boolean"},
    }),
});
