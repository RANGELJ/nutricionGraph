const validator = require("./validatorPaciente.gph");

module.exports = {
    type: "@Mutation",
    sqlfiles: ["updatePaciente"],
    args: {
        id: {type: "@NonNull(Int)"},
        nombre: {type: "@NonNull(String)"},
        apellidoPaterno: {type: "@NonNull(String)"},
        apellidoMaterno: {type: "@NonNull(String)"},
        fechaNacimiento: {type: "@NonNull(String)"},
        telefono: {type: "@NonNull(String)"},
        idEstadoCivil: {type: "@NonNull(Int)"},
        sexo: {type: "@NonNull(String)"},
        idColonia: {type: "@NonNull(Int)"},
        calle: {type: "@NonNull(String)"},
        numeroCalle: {type: "@NonNull(String)"},
        referencias: {type: "@NonNull(String)"},
    },
    async resolve(root, args, context) {
        // @Error al editar el paciente
        await validator(root, args, context);
        const conn = await context.db("nutricion");
        let params = [args.id];
        const [idsAux] = await conn.execute("sql@getIds", params);
        const idDireccion = idsAux[0].idDireccion;
        const idGenerales = idsAux[0].idGenerales; // eslint-disable-line
        await conn.execute("sql@updateGenerales", [
            args.nombre, args.apellidoPaterno, args.apellidoMaterno,
            args.fechaNacimiento, args.telefono, args.idEstadoCivil,
            args.sexo, idGenerales,
        ]);
        await conn.execute("sql@updateDireccion", [
            args.idColonia, args.calle, args.referencias,
            args.numeroCalle, idDireccion,
        ]);
        return {id: idDireccion};
    },
};
