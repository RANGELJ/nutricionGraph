module.exports = {
    type: "@IntensidadFisicaPorEdad",
    sqlfiles: ["getIntensidadFisicaPorEdad"],
    args: {
        minimo: {type: "@Int"},
        maximo: {type: "@Int"},
        idIntensidad: {type: "@Int"},
    },
    async resolve(root, args, context) {
        // @Error al consultar las intensidades fisicas por edad
        const conn = await context.db("nutricion");
        const [intensidades] = await conn.execute("sql@intensidad", [
            args.idIntensidad, args.minimo, args.maximo,
        ]);
        return {total: intensidades.length};
    },
};
