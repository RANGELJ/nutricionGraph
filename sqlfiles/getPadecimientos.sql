
-- @conteoPadecimiento
SELECT
	padecimiento.id AS idPadecimiento,
    (SELECT COUNT(*) FROM padecimiento_paciente
		WHERE idPaciente = ?
        AND idPadecimiento = padecimiento.id) AS cantidad
FROM
	padecimientos padecimiento
-- @

-- @insertRelacion
INSERT INTO padecimiento_paciente
    (idPadecimiento, idPaciente, activo)
    VALUES
    (?, ?, b'0')
-- @

-- @padecimientos
SELECT
	id,
    nombre
FROM
	padecimientos
WHERE
	activo = 1
-- @

-- @padecimientoPaciente
SELECT
	relacion.id,
    padecimiento.nombre,
    relacion.activo
FROM
	padecimiento_paciente relacion
JOIN
	padecimientos padecimiento ON (
		padecimiento.id = relacion.idPadecimiento
    )
WHERE
	relacion.idPaciente = ?
-- @
