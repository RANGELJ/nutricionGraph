module.exports = {
    type: "@List(ConsumoPromedio)",
    sqlfiles: ["getConsumoPromedio"],
    async resolve(root, args, context) {
        // @Error al consultar los consumos por categoria
        const conn = await context.db("nutricion");
        const [consumos] = await conn.execute("sql@promedioCats");
        return consumos.map(consumo => {
            consumo.consumo = consumo.sumaFrecuencias
                / consumo.totalElementos;
            return consumo;
        });
    },
};
