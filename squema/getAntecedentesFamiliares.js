module.exports = {
    type: "@List(AntecedenteFamiliar)",
    sqlfiles: ["getAntecedentesFamiliares"],
    args: {
        idPaciente: {type: "@Int"},
    },
    async resolve(root, args, context) {
        // @Error al consultar los antecedentes familiares
        const conn = await context.db("nutricion");

        if (args.idPaciente) {
            const [counters] = await conn.execute("sql@contador", [
                args.idPaciente,
            ]);
            counters.forEach(async counter => {
                if (counter.cantidad == "0") {
                    await conn.execute("sql@insertRelacion", [
                        counter.idAntecedente, args.idPaciente,
                    ]);
                }
            });

            const [antecedentes]
                = await conn.execute("sql@antecedentePaciente", [
                    args.idPaciente,
                ]);
            return antecedentes.map(antecedente => {
                antecedente.activo
                    = antecedente.activo === 1;
                return antecedente;
            });
        } else {
            const [antecedentes] = await conn.execute("sql@antecedentes");
            return antecedentes;
        }
    },
};
