
-- @indicadores
SELECT
	id,
	comidasAlDia,
    idPreparaAlimentos,
    idTipoApetito,
    idHoraMasHambre,
    cantidadVasosAguaDia,
    cantidadVasosBebidas
FROM
	indicadores_dieteticos
WHERE
	idPaciente = ?
-- @

-- @nuevoRegistro
INSERT INTO indicadores_dieteticos
    (idPaciente)
VALUES
    (?)
-- @
