
module.exports = {
    resolveWrapper(resolve, publicErrorMessage) {
        return async function(root, args, context) {
            try {
                return await resolve(root, args, context);
            } catch (error) {
                /**
                 * Si tiene la bandera noLog,
                 * manda el mensaje directo al cliente,
                 * muy usado en validaciones
                 */
                if (error.noLog === true) {
                    throw error.message;
                } else {
                    context.logError(error.message);
                    throw publicErrorMessage;
                }
            }
        };
    },
};
